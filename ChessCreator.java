
import java.util.Random;
import java.util.Arrays;  
import java.util.List;  
import java.util.ArrayList;  



public class ChessCreator {

	public class Piece {
		public int x;
		public int y;
		public String type;

		public Piece (int inX, int inY, String inType) {
			x = inX;
			y = inY;
			type = inType;
		}
	}

	public int totalSolutions;
	public int totalMoves;
    public String[] movePiece;
    public int current = 0;
	public String[][] initialArray;
    public int difficulty = 0;
    public String[] foundSolution;

    public static void main(String[] args) {
    	int difficulty = Integer.parseInt(args[0]);
    	int numPieces = difficulty;
    	ChessCreator creator = new ChessCreator();
       	creator.createChessPuzzle(numPieces);
		while (creator.totalSolutions != 1 || creator.totalMoves < numPieces * numPieces * numPieces || creator.boringSolution()) {
			creator.createChessPuzzle(numPieces);
		}
		System.out.println(creator.totalMoves);
		creator.printBoard(creator.initialArray);
        if (args.length > 1 && args[1] != null && args[1].equalsIgnoreCase("hint")) {
         for (int j=0;j<creator.difficulty;j++) {
             System.out.println(creator.foundSolution[j]);
         }
        }
    }

    public boolean boringSolution() {
        String test = "";
        for (int j=0;j<this.difficulty;j++) {
            test = test + this.foundSolution[j];
        }
        if (test.contains("RRR") || test.contains("QQQ") || test.contains("KKK") || test.contains("BBB")) {
            return true;
        }
        return false;
    }

  public void createChessPuzzle (int difficulty) {
        this.difficulty = difficulty;
    	totalSolutions = 0;
    	totalMoves = 0;
    	String[] piecesArray = {"K", "Q", "P", "P", "R", "R", "B", "B", "H", "H"};
    	ArrayList<String> pieces = new ArrayList<>(Arrays.asList(piecesArray));  
        String[][] array = new String[4][4];

        for (int i=0;i<4;i++) {
        	for (int j=0;j<4;j++) {
        		array[i][j] = " ";
        	}
        }

        for (int i = 0;i<difficulty;i++) {
        	Random ran = new Random();
        	int chosenPiece = ran.nextInt(pieces.size());
        	String piece = pieces.remove(chosenPiece);
        	int x = ran.nextInt(4);
        	int y = ran.nextInt(4);
        	while (array[x][y] != " ") {
        		x = ran.nextInt(4);
        		y = ran.nextInt(4);
        	}
        	array[x][y] = piece;
        }
        initialArray = cloneArray(array);
        current = 0;
        movePiece = new String[2000];
        foundSolution = new String[difficulty];
        findSolution(array);
    }

    public void printBoard (String[][] array) {
    	System.out.println("+----+");
        for (int i=0;i<4;i++) {
        	String row = "|";
        	for (int j=0;j<4;j++) {
        		row += array[i][j];
        	}
        	row += "|";
        	System.out.println(row);
        }
        System.out.println("+----+");
    }

    public void findSolution (String[][] array) {
    	totalMoves++;
    	List<Piece> pieceList = new ArrayList<Piece>();

		for (int i=0;i<4;i++) {
        	for (int j=0;j<4;j++) {
        		if (array[i][j] != " ") {
        			Piece newPiece = new Piece(i, j, array[i][j]);
        			pieceList.add(newPiece);
        		}
        	}
        }

        if (pieceList.size() == 1) {
        	totalSolutions++;
            int x = 0;
            for (int j=current-difficulty+1;j<current;j++) {
                foundSolution[x] = movePiece[j];
                x++;
            }
        } else {
        	for (Piece piece : pieceList) {
    			movePiece(array, piece);
			}
        }
        current--;
        return;
    }

    private void movePiece (String[][] array, Piece piece) {
    	if (piece.type == "R" || piece.type == "Q") {
    		//piece directly left
    		for (int x = 1; piece.x-x >= 0;x++) {
    			if (array[piece.x-x][piece.y] !=  " ") {
    				makeMove(piece, piece.x-x, piece.y, array);
    				x = 10;
    			}
    		}
    		//right
    		for (int x = 1; piece.x+x <= 3;x++) {
    			if (array[piece.x+x][piece.y] !=  " ") {
    				makeMove(piece, piece.x+x, piece.y, array);
    				x = 10;
    			}
    		}
    		//down
    		for (int x = 1; piece.y+x <= 3;x++) {
    			if (array[piece.x][piece.y+x] !=  " ") {
    				makeMove(piece, piece.x, piece.y+x, array);
    				x = 10;
    			}
    		}
    		//up
    		for (int x = 1; piece.y-x >= 0;x++) {
    			if (array[piece.x][piece.y-x] !=  " ") {
    				makeMove(piece, piece.x, piece.y-x, array);
    				x = 10;
    			}
    		}
    	} if (piece.type == "H") {
    		int[][] moves = {{-2, -1}, {-1, -2}, {1, -2}, {2, -1}, {2, 1}, {1, 2}, {-1, 2}, {-2, 1}};
    		for (int i = 0;i<8;i++) {
    			int x = piece.x+ moves[i][0];
    			int y = piece.y+ moves[i][1];
    			if (x <=3 && x >=0 && y <= 3 && y >= 0) {
    				if (array[x][y] !=  " ") {
						makeMove(piece, x, y, array);
    				} 
				}
    		}
    	} if (piece.type == "P") {
    		int[][] moves = {{-1, -1}, {-1, 1}};
    		for (int i = 0;i<2;i++) {
    			int x = piece.x+ moves[i][0];
    			int y = piece.y+ moves[i][1];
    			if (x <=3 && x >=0 && y <= 3 && y >= 0) {
    				if (array[x][y] !=  " ") {
						makeMove(piece, x, y, array);
					}
				}
    		}
    	} if (piece.type == "K") {
    		int[][] moves = {{-1, -1}, {-1, 0}, {-1, -1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1}};
    		for (int i = 0;i<8;i++) {
    			int x = piece.x+ moves[i][0];
    			int y = piece.y+ moves[i][1];
    			if (x <=3 && x >=0 && y <= 3 && y >= 0) {
    				if (array[x][y] !=  " ") {
    					makeMove(piece, x, y, array);
    				} 
				}
    		}	
    	} if (piece.type == "Q" || piece.type == "B") {
    		//down-right
    		for (int x = 1; piece.x+x <= 3 && piece.y+x <= 3;x++) {
    			if (array[piece.x+x][piece.y+x] !=  " ") {
    				makeMove(piece, piece.x+x, piece.y+x, array);
    				x = 10;
    			}
    		}
    		//up right
    		for (int x = 1; piece.x-x >=0 && piece.y+x <= 3;x++) {
    			if (array[piece.x-x][piece.y+x] !=  " ") {
    				makeMove(piece, piece.x-x, piece.y+x, array);
    				x = 10;
    			}
    		}
    		//down left
    		for (int x = 1; piece.x+x <= 3 && piece.y-x >= 0;x++) {
    			if (array[piece.x+x][piece.y-x] !=  " ") {
    				makeMove(piece, piece.x+x, piece.y-x, array);
    				x = 10;
    			}
    		}
    		//up left
    		for (int x = 1; piece.x-x >= 0 && piece.y-x >= 0;x++) {
    			if (array[piece.x-x][piece.y-x] !=  " ") {
    				makeMove(piece, piece.x-x, piece.y-x, array);
    				x = 10;
    			}
    		}
    	}
    	return;
    }

    private void makeMove(Piece piece, int x, int y, String[][] array) {
        movePiece[current] = piece.type;
        current++;
    	String[][] newArray = cloneArray(array);
    	newArray[x][y] = piece.type;
    	newArray[piece.x][piece.y] = " ";
    	findSolution(newArray);
    }

    public String[][] cloneArray(String[][] src) {
	    int length = src.length;
	    String[][] target = new String[length][src[0].length];
	    for (int i = 0; i < length; i++) {
	        System.arraycopy(src[i], 0, target[i], 0, src[i].length);
	    }
	    return target;
	}

}
